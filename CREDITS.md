# Credits
These are the people that have worked on Heb12 Desktop. This file is only to be edited on terms of the "Credits" section of CONTRIBUTING.md. 

## Development Team
- MasterOfTheTiger (lead developer, built the majority of the program)
- AmazinigMech2418 (front-end developer, online version)
- Doodthedoodthedood (front-end developer, red and light blue themes)

## Significant Contributors

## Other Contributors
